# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
GeneaScraper::Application.config.secret_key_base = '805f9328fcd1b11332dddf0c88d319107b8483450b622123ff5e4d7162c6ddf9b47a747847e4f365f30b7c49d93732f2631e1d858185bb241b23ccf2503f071b'
