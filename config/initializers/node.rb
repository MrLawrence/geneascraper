module Nokogiri
  module XML
    class Node
      def text_at(css)
        at_css(css).text rescue ''
      end
    end
  end
end
