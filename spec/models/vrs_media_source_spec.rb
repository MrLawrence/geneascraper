require 'spec_helper'

describe VrsMediaSource do
  describe '#find' do
    let :source1 do
      {
        domain: 'trauer.de',
        url: 'http://www.trauer.de',
        search_url: 'https://www.trauer.de/Traueranzeigen/suchen/_/_/_/0/0/1'
      }
    end
    before(:each) do
      Docs::Source.create(source1)

      @search = VrsMediaSource.new
    end

    context 'getting data' do
      it 'returns deceaseds' do
        results = @search.find('Madl', 'Anni', 'trauer.de')

        expect(results).to be_an_instance_of(Array)
        expect(results.size).to eq(1)

        deceased = results.first

        expect(deceased).to be_timestamped_document

        expect(deceased.full_name).to eq('Anni Madl')

        expect(deceased.notice_place).to eq('Benediktbeuern')
        expect(deceased.birthday).to eq(Date.parse('1932-03-13'))
        expect(deceased.deathday).to eq(Date.parse('2014-08-22'))

        expect(deceased.source_url).to eq('http://merkurtz.trauer.de/Traueranzeige/Anni-Madl')
        expect(deceased.source).to eq('merkurtz.trauer.de')
      end
    end
  end
end
