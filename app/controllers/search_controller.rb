class SearchController < ApplicationController
  def index
    @deceased_count = Docs::Deceased.count
    @source_count = Docs::Source.count
    @sources = Docs::Source.all.map(&:domain)
  end

  def deceased
    @results = VrsMediaSource.new.find(params[:last_name], params[:first_name], params[:source])
    respond_to do |format|
      format.html { render :deceaseds }
      format.xml { render xml: @results }
      format.json { render json: @results }
    end
  end

  def db
    @results = Docs::Deceased.any_of(last_name: /#{params[:last_name]}.*/)
    respond_to do |format|
      format.html { render :deceaseds }
    end
  end
end
