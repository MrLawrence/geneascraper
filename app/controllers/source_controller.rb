class SourceController < ApplicationController
  def index
    @sources = Docs::Source.all

    respond_to do |format|
      format.html { render :index }
    end
  end

  def add
    Sources::VrsMedia::Source.new.add(params[:url], params[:search_url])
    redirect_to :back
  end
end
