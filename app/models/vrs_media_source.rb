# frozen_string_literal: true

class VrsMediaSource
  def initialize
    @search = Sources::VrsMedia::Search.new
  end

  def find(last_name, first_name, domain)
    first_name = '_' if first_name == ''
    source = Docs::Source.find_by(domain: domain)
    url = source.search_url.gsub('/_/_/_/', "/#{URI.escape(first_name)}/#{URI.escape(last_name)}/_/")
    deceaseds = @search.search(url)
    save_new_deceaseds(deceaseds)
  end

  def results_by_date(start_date, end_date, domain)
    source = Docs::Source.find_by(domain: domain)
    url = source.search_url.gsub('/0/0/', "/0/#{start_date}/#{end_date}/")
    deceaseds = @search.search(url)
    save_new_deceaseds(deceaseds)
  end

  def search(last_name, first_name)
    sources = Docs::Source.all
    domains = sources.all.map(&:domain)

    thread_amount = 2
    workload = domains.in_groups(thread_amount).to_a

    threads = workload.map do |chunk_domains|
      Thread.new do
        chunk_domains.map do |domain|
          find(last_name, first_name, domain)
        end
      end
    end

    result = []
    threads.each do |t|
      result << t.value
    end
    result.flatten
  end

  private

  def save_new_deceaseds(deceaseds)
    # splitting into known deceaseds (in db) and unknown deceaseds
    known = deceaseds.select  { |deceased|  Docs::Deceased.where(source_url: deceased.source_url).exists? }
    unknown = deceaseds.reject { |deceased|  Docs::Deceased.where(source_url: deceased.source_url).exists? }
    Rails.logger.info "Saved #{unknown.count} new deceaseds to database"
    unknown.each(&:insert)
    updated_known = known.map do |deceased|
      Docs::Deceased.find_by(source_url: deceased.source_url)
    end
    unknown.concat(updated_known)
  rescue  Mongoid::Errors::DocumentNotFound => e
    Rails.logger.warn("Domain not found in database: #{domain}")
    raise e
  end
end
