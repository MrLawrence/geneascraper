# frozen_string_literal: true

  module Docs
    # used for saving any images found, mostly death notices and portraits
    class Image
      include Mongoid::Document
      include Mongoid::Timestamps

      field :hash, type: String
      field :url, type: String
      field :description, type: String

      index({ url: 1 }, { unique: true })
      has_and_belongs_to_many :deceased, class_name: 'Docs::Deceased'
    end
  end
