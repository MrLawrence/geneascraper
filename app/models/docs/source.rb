# frozen_string_literal: true

  module Docs
    class Source
      include Mongoid::Document
      include Mongoid::Timestamps

      field :domain, type: String
      field :url, type: String
      field :search_url, type: String

      index({ domain: 1 }, { unique: true })
    end
  end
