# frozen_string_literal: true

module Docs
  # represents the deceased person for mongodb
  class Deceased
    include Mongoid::Document
    include Mongoid::Timestamps

    field :first_names, type: Array, default: []
    field :last_name, type: String
    field :maiden_name, type: String
    field :full_name, type: String

    field :birthday, type: Date
    field :deathday, type: Date

    field :notice_place, type: String
    field :source_url, type: String
    field :source, type: String

    field :entry_creator, type: String
    field :entry_date, type: Date

    field :result_page_scraped, type: Boolean, default: false
    field :deceased_page_scraped, type: Boolean, default: false
    field :notice_page_scraped, type: Boolean, default: false

    has_and_belongs_to_many :images, class_name: 'Docs::Image'
    accepts_nested_attributes_for :images, class_name: 'Docs::Image'

    index({ source_url: 1 }, { unique: true })
    validates_presence_of :source_url
  end
end
