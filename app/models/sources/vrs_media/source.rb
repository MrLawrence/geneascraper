# frozen_string_literal: true

require 'public_suffix'
module Sources
  module VrsMedia
    class Source
      def add(url, search_url)
        domain = PublicSuffix.parse(url.gsub('http://', '')).subdomain.gsub('www.', '')
        if Docs::Source.where(domain: domain).exists?
          false
        else
          source = Docs::Source.new
          source.domain = domain
          source.url = url
          source.search_url = search_url
          source.insert
          true
        end
      end
    end
  end
end
