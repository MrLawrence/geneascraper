# frozen_string_literal: true

module Sources
  module VrsMedia
    class Search
      def initialize
        @requester = Sources::VrsMedia::Requester.new
        @result_scraper = Sources::VrsMedia::Scraper::Results.new(@requester)
        @info_scraper = Sources::VrsMedia::Scraper::Info.new(@requester)
        @notice_scraper = Sources::VrsMedia::Scraper::DeathNotice.new(@requester)
      end

      def search(url)
        Rails.logger.info "Searching on #{url}"

        deceaseds = @result_scraper.scrape(url)

        Rails.logger.info "Downloaded #{deceaseds.count} result pages"
        Rails.logger.info "#{@requester.request_count} requests"

        deceaseds
      end

      def get_info(deceaseds)
        @info_scraper.scrape(deceaseds)
      end
    end
  end
end
