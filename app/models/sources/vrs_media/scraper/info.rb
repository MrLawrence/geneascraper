# frozen_string_literal: true

##
# an info page is the direct URL to the deceased persons entry
#
module Sources
  module VrsMedia
    module Scraper
      class Info
        def initialize(requester)
          @req = requester
          @info_parser = Sources::VrsMedia::Parser::Info.new
          @notice_parser = Sources::VrsMedia::Parser::DeathNotice.new
        end

        def scrape(deceaseds)
          html_dec = @req.request_info(deceaseds)
          html_dec.each do |deceased, response|
            #fail Exceptions::NotFoundError if @req.not_found?(response.response.body) || !response.response.success?

            @info_parser.parse(response.response.body, deceased)
            deceased.deceased_page_scraped = true
            Rails.logger.info "#{deceased.birthday.year rescue 0} - #{deceased.deathday.year rescue 0} #{deceased.first_names.join(' ')} #{deceased.last_name} Info scraped"
          end
          true
        end
      end
    end
  end
end
