# frozen_string_literal: true

module Sources
  module VrsMedia
    module Scraper
      class DeathNotice
        def initialize(requester)
          @req = requester
          @notice_parser = Sources::VrsMedia::Parser::DeathNotice.new
        end

        def scrape(deceased, info)
          response = @req.request_details(info[:url], info[:content], info[:cookie])
          @notice_parser.parse(response, deceased)
        end
      end
    end
  end
end
