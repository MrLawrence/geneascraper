# frozen_string_literal: true

##
# A result page is one of probably many pages that were returned in response of a search
#
module Sources
  module VrsMedia
    module Scraper
      class Results
        def initialize(requester)
          @req = requester
          @type_a_parser = Sources::VrsMedia::Parser::ResultsTypeA.new
          @type_b_parser = Sources::VrsMedia::Parser::ResultsTypeB.new
          @parser = nil
        end

        def scrape(url)
          response = @req.request(url)
          #fail Exceptions::NotFoundError if @req.not_found?(response.body) || !response.success?
          if @type_a_parser.valid?(response.body)
            @parser = @type_a_parser
          else
            @parser = @type_b_parser
          end

          deceaseds = @parser.parse(response.body)

          deceaseds.each { |deceased| deceased.result_page_scraped = true }
          page_amount = @parser.page_count(response.body)
          Rails.logger.info "Page Amount: #{page_amount}"

          if page_amount > 1
            urls = generate_urls(url, page_amount)
            @req.request_result(urls).each do |page|
              result = @parser.parse(page.response.body)
              deceaseds << result
            end
          end

          deceaseds.flatten.each do |page_dec|
            Rails.logger.info "#{page_dec.birthday.year rescue 0} - #{page_dec.deathday.year rescue 0} #{page_dec.full_name} result scraped"
          end
          deceaseds.flatten
        end

        private

        def generate_urls(url, page_amount)
          urls = []
          2.upto(page_amount) do |page_number|
            urls << url[0..-2] + page_number.to_s
          end
          urls
        end
      end
    end
  end
end
