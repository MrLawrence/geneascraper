# frozen_string_literal: true

module Sources
  module VrsMedia
    module Parser
      class DeathNotice
        def parse(response, deceased)
          Rails.logger.debug "Parsing death notice for #{deceased.first_names.join(' ')} #{deceased.last_name}"

          html = Nokogiri::HTML.parse(response.body)
          html.css('a[id*="btnGrossansicht"]').each do |anzeige|
            image = Docs::Image.new
            image.url = anzeige['onclick'][/\ '(.*)'\);/, 1]
            image.description = 'death notice'
            deceased.images << image unless image.url.nil?
          end
        end
      end
    end
  end
end
