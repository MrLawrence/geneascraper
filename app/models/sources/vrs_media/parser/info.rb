# frozen_string_literal: true

##
# parses the page of a death notice
#

module Sources
  module VrsMedia
    module Parser
      class Info
        def parse(raw_html, deceased)
          Rails.logger.debug "Parsing details for #{URI.encode deceased.source_url}"

          html = Nokogiri::HTML.parse(raw_html)

          image_details = html.at_css('div#ImageDetails')

          image = Docs::Image.new
          image.url = image_details.at_css('img')['src'] rescue ''
          image.description = 'portrait'

          deceased.images << image unless image.url.nil?
          deceased.first_names = image_details.at_css('span#firstName1nd').text.split(' ')

          name = image_details.at_css('span#lastName1nd').text
          if name =~ /geb./
            splitted_name = name.split(', geb. ')
            deceased.last_name = splitted_name[0]
            deceased.maiden_name = splitted_name[1]
          else
            deceased.last_name = name
          end

          birthday_raw = get_date_text(image_details.at_css('span#birthDay'))

          deceased.birthday = format_time(birthday_raw)

          deathday_raw = get_date_text(image_details.at_css('span#deathDay'))
          deceased.deathday = format_time(deathday_raw)

          user_info = html.at_css('div.userInfo')

          deceased.entry_creator = user_info.text_at('a#creatorName')
          entry_date_raw = user_info.text_at('p#creationDate')[/Angelegt\ am\ (.*?)$/, 1]
          deceased.entry_date = format_time(entry_date_raw)

          base_url = html.at_css('div#site header.Topmenu a')['href']
          url = base_url + '/Microsite/Views/Apps/Startpage/Startpage.aspx'

          content = html.to_s[/index.aspx\?(.*?)"/, 1]

          { url: url, content: content }
        end

        private

        def get_date_text(nodeset)
          nodeset.search('span.birthDayStyle').remove
          nodeset.text.strip
        end

        def format_time(time)
          Date::strptime(time, '%d.%m.%Y').to_s
        rescue ArgumentError, TypeError
          Rails.logger.warn "Invalid Date scraped: '#{time}'"
          nil
        end
      end
    end
  end
end
