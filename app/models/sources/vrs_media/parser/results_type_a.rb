# frozen_string_literal: true

##
# Parses result pages of type A, which is an arbitrary name for a result page type.
module Sources
  module VrsMedia
    module Parser
      class ResultsTypeA
        def parse(body)
          Rails.logger.debug "Parsing search results type A from #{body.bytesize} Byte page"

          html = Nokogiri::HTML.parse(body)
          deceaseds = html.css('div.searchItemMainPanel').map do |item|
            deceased = Docs::Deceased.new
            deceased.full_name = item.text_at('a[id*="HyperLinkName"]')
            deceased.notice_place = item.text_at('span[id*="LabelCity"]')

            date_label = item.text_at('span[id*="DateLabel"]')
            rescuer('Birthday') { deceased.birthday = format_time(date_label[/\* ([\d\.]*?)[\ $]/, 1]).to_s }
            rescuer('Deathday') { deceased.deathday = format_time(date_label[/† ([\d\.]*?)$/, 1]).to_s }

            deceased.source_url = item.at_css('div.LabelPanelSearchIssue h2 a')['href']
            deceased.source = URI.parse(URI.encode(deceased.source_url)).host
            deceased
          end
          Rails.logger.debug "Found #{deceaseds.size} deceaseds in page"
          deceaseds
        end

        ##
        # Returns the amount of result pages
        def page_count(body)
          html = Nokogiri::HTML.parse(body)
          result_count = html.text_at('span#ctl00_ContentPlaceHolder1_lbl_SearchParameters')[/Ihnen\ (.*?)$/, 1].to_i
          result_count / 5 + 1 # 5 results per page
        end

        ##
        # check if the html page is parsable by this class
        def valid?(body)
          html = Nokogiri::HTML.parse(body)
          !!html.at_css('div.searchItemMainPanel')
        end

        private

        def rescuer(tag)
          yield
        rescue NoMethodError
          Rails.logger.warn "Parsing #{tag} failed!"
        end

        def format_time(time)
          Date.parse(time, '%d.%m.%Y').to_s rescue nil
        end
      end
    end
  end
end
