# frozen_string_literal: true


##
# Parses result pages of type B, which is an arbitrary name for a result page type.

module Sources
  module VrsMedia
    module Parser
      class ResultsTypeB
        def parse(body)
          Rails.logger.debug "Parsing search results type B from #{body.bytesize} Byte page"

          html = Nokogiri::HTML.parse(body)
          deceaseds = html.css('div.searchItemForTabModule').map do |item|
            deceased = Docs::Deceased.new
            deceased.full_name = item.text_at('a.searchTitle')[/^Traueranzeige (.*?)$/, 1]

            date_label = item.text_at('span[id*="LabelDateInfos"]')
            rescuer('Birthday') { deceased.birthday = format_time(date_label[/\* ([\d\.]*?)[\ $]/, 1]).to_s }
            rescuer('Deathday') { deceased.deathday = format_time(date_label[/† ([\d\.]*?)$/, 1]).to_s }


            deceased.source_url = item.at_css('a.searchTitle')['href']
            deceased.source = URI.parse(URI.encode(deceased.source_url)).host
            deceased
          end
          Rails.logger.debug "Found #{deceaseds.size} deceaseds in page"
          deceaseds
        end

        ##
        # Returns the amount of result pages
        def page_count(body)
          html = Nokogiri::HTML.parse(body)
          result_count = html.text_at('span#ctl00_ContentPlaceHolder1_lbl_SearchParameters')[/Ihnen\ (.*?)$/, 1].to_i
          result_count / 5 + 1  # 5 results per page
        end

        ##
        # check if the html page is parsable by this class
        def valid?(body)
          Rails.logger.severe 'Not implemented'
          false
        end

        private

        def rescuer(tag)
          yield
        rescue NoMethodError
          Rails.logger.warn "Parsing #{tag} failed!"
        end

        def format_time(time)
          Date.parse(time, '%d.%m.%Y').to_s rescue nil
        end
      end
    end
  end
end
