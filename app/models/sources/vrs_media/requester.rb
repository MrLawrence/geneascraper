# frozen_string_literal: true

module Sources
  module VrsMedia
    class Requester
      include Requestable

      def not_found?(html)
        !!(html.to_s =~ /Seite existiert nicht/)
      end

      def request_details(uri, content, cookie)
        headers = { 'Cookie' => cookie }
        post(uri, content, headers)
      end

      def request_info(deceaseds)
        hydra = Typhoeus::Hydra.new(max_concurrency: 10)

        return {} if deceaseds.empty?

        responses = {}

        deceaseds.each do |deceased|
          responses[deceased] = Typhoeus::Request.new(URI.encode(deceased.source_url), followlocation: true)
          hydra.queue responses[deceased]
        end
        hydra.run
        responses
      end

      def request_result(urls)
        hydra = Typhoeus::Hydra.new(max_concurrency: 10)

        responses = []
        urls.each do |url|
          request = Typhoeus::Request.new(URI.encode(url))
          responses << request
          hydra.queue request
        end
        hydra.run
        responses
      end
    end
  end
end
