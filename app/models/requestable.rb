# frozen_string_literal: true

module Requestable
  attr_reader :request_count

  def initialize
    @random = Random.new
    @delay = 0.4
    @request_count = 0
  end

  def request(url)
    response = get(URI.encode(url))
    handle_response(response)
  end

  def get(url)
    random_wait
    @request_count += 1
    Rails.logger.info 'Requesting...'
    response = Typhoeus.get(URI.encode(url), timeout: 60)
    Rails.logger.info 'Response acquired'
    response
  end

  def post(url, content, headers)
    @request_count += 1
    random_wait
    uri = URI(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.read_timeout = 10
    http.post(uri.path, content, headers)
  rescue Net::ReadTimeout
    Rails.logger.error "Timeout for #{url} with content #{content}"
  end

  private

  def handle_response(response)
    case response
    when Net::HTTPMovedPermanently
      redirect_url = response['location']
      request(redirect_url)
    else
      response
    end
  end

  def random_wait
    min = 0.8 * @delay
    max = 1.2 * @delay
    rand = @random.rand(min..max)
    sleep(rand)
  end
end
